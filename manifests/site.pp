# if the user supplies the mining_profile from the rig we use that data which is stored in a fact
# the fact is backed by a settings file so the rig should never change unless the mining_profile fact changes to default
# which then causes the remote lookup to be performed.  If the user supplies the word remote, then the remote mining_profile will be used
$remote_rig_owner = lookup('crossbelt::rig_owner', {default_value => 'default'})
if ! $::facts['rig_owner_local'] or $::facts['rig_owner_local'] == 'remote' or $::facts['rig_owner_local'] == 'default'  {
  $rig_owner = $remote_rig_owner
} else {
  $rig_owner = $::facts['rig_owner_local']
}

$remote_mining_profile = lookup('crossbelt::mining_profile', {default_value => 'default'})
if ! $::facts['mining_profile_local'] or $::facts['mining_profile_local'] == 'remote' or $::facts['mining_profile_local'] == 'default'  {
  $mining_profile = $remote_mining_profile
} else {
  $mining_profile = $::facts['mining_profile_local']
}

# set the algo profile so hiera can lookup data, this requires the mining_profile to be set
$algo_profile = lookup('crossbelt::config::algo_profile', {default_value => 'default'})

include crossbelt
