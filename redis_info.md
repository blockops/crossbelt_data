```
- name: hiera_redis
    lookup_key: redis_lookup_key
    options:
      confine_to_keys:
        - 'crossbelt::mining_profile'
        - 'crossbelt::rig_owner'
        - 'crossbelt::config::rig::loc'
        - 'crossbelt::wallets::.*'
        - 'crossbelt::config::.*'
        - 'crossbelt::metrics::power_cost'
        - 'crossbelt::metadata::mining_site'
      scopes:
        - "rig_owner/%{rig_owner}"
        - "rigs/%{facts.hostname}"
        - "mining_sites/%{mining_facility_name}"

```
