# How to Use

All of these settings will forge a configuration file for ethos to use.  This is the initial version of Crossbelt and the 
production cloud version will negate all of this. However, until the cloud version is ready you can use this file based system 
in the meaantime. Follow the procedures below to auto configure your rig using the information contained in the project.

Below are the steps to start using Crossbelt. By default your rig will get a default configuration so nothing really
needs to be done.  However, if you want to mine to your wallets instead of mine you need to create a a rig owner 
file for yourself.

Each of the files you create are called YAML files and can contain comments if the line starts with `#`

This data is what is known as hiera (hi ruh) data and is part of the puppet tooling that Crossbelt utilizes.  
Official [hiera documention](https://puppet.com/docs/puppet/6.2/hiera_intro.html) can be found here.  If you fully understand
hiera you can do more clever things with this data ;)

## Create a rig owner
Unless a rig owner already exists for the rig you added do the following.  You will only need wallet info 
for the currency you wish to mine.  

1. Create a file in data/rig_owner/owner_name.yaml
2. optionally populate the file with wallet info, defaults will apply if left blank

```yaml
crossbelt::wallets::btc: 
crossbelt::wallets::etn: 
crossbelt::wallets::dcr: 
crossbelt::wallets::eth: 
crossbelt::wallets::zec: 
crossbelt::wallets::etc: 
crossbelt::wallets::xmr: 
crossbelt::config::poolemail: 'your email address for notifications'

# look a comment line!

```

## Get the hostname of your Rig
1. From the command line on your rig run `hostname` which should spit out something like `000000`
2. Create a file in data/rigs/ with your hostname as the file name  ie. `data/rigs/000000.yaml`
3. Add a mining profile 
4. Add a rig owner
5. Add a loc name (alias for your rig) # ethos only at this time
6. Add facility name (where your rig is)

Your rig file should look something like

```yaml
crossbelt::mining_profile: equihash_nicehash
crossbelt::rig_owner: opselite
crossbelt::config::rig::loc: 'zenn6'
```

Note: defaults will apply if left blank

## Create a facility name
Unless already created you can add new faclity names by doing the following
1. Create a file like `data/mining_sites/some_factity_name.yaml`
2. Optionally, inside the file add the following content

```yaml
crossbelt::metrics::power_cost: 0.09 # the default is `0.10`

```

This is the cost of power at the facility and will vary per facility.  

If you want to set a default mining profile for all your rigs this is the place to do it.

`crossbelt::mining_profile: equihash_nicehash`

Otherwise your rig will override this data.


## Create GPU Profiles
A gpu profile is a gpu bios level profile that contains various Overclock settings
for each gpu bios.  

**NOTE: if you change a gpu bios config you change it for everyone using this repo!**

You can override the bios configs by placing the same content in your rig file and adjust to your needs.

To get started create a file under `data/gpus/<type>/bios-name.yaml`

You can get the bios names by running `crossbelt gpu info`

Example file and content:

ie. data/gpus/amdgpu/113-1E366CU-S52.yaml

```yaml
## Global Configs
crossbelt::config::globalmem: 1900
crossbelt::config::globalcore: 1150

## Default Configs that override the global configs above
crossbelt::config::vlt: 900
crossbelt::config::cor: 1150
crossbelt::config::mem: 1950
crossbelt::config::pwr: 3
```


You can further override this setting on a per algorithm level by creating a directory starting with the bios
name and a file with the currency name.  This would have the same keys but different values as they would override
the default bios settings for that specific currency.

ie. `data/gpus/amdgpu/113-1E366CU-S52/eth.yaml`

```yaml
# eth likes a higher core clock
crossbelt::config::cor: 1350
crossbelt::config::mem: 1950
crossbelt::config::pwr: 5
crossbelt::config::desc: 'Asus 570 4GB'

```

### Custom OC profiles
But wait!  There's more.  You can also create unique one-off OC profiles like above, but with any kind of name.
However, you will need to also associate your bioses with these oc profiles.

So you create a oc profile like `data/gpus/amdgpu/113-1E366CU-S52/eth_test_2300.yaml`
and set this in the `crossbelt::config::gpu_configs`

```yaml
crossbelt::config::gpu_configs:
  '113-1E366CU-S52': eth_test_2300
  '86.04.50.00.70': equihash
  '86.04.85.00.63': crazy_overclocked_equihash

```

The OC profile

```yaml
# data/gpus/amdgpu/113-1E366CU-S52/eth_liquid_cooled_2300.yaml
crossbelt::config::cor: 1150
crossbelt::config::mem: 2300
crossbelt::config::pwr: 5
crossbelt::config::desc: 'Asus 570 4GB'
# must have liquid cooling to run 2300 memory
```

However, be careful where you put the config above.  If your testing on a single rig put this config in the rig data only.
If you want it for all your rigs, put in the rig owner file.


```yaml
# only apply 2300 memory overclock to rig 000000
# data/rigs/000000.yaml
crossbelt::config::gpu_configs:
  '113-1E366CU-S52': eth_test_2300
  '86.04.50.00.70': equihash
  '86.04.85.00.63': crazy_overclocked_equihash
```

## Creating a mining profile
A mining profile is a combination of various things that include

* enable dual mining
* which pools to mine to
* which currency
* other settings that override other data

ie. `data/mining_profiles/ethermine.yaml`

```yaml
crossbelt::rig::dualminer_enabled: false
crossbelt::rig::pool_name: 'ethermine'
crossbelt::rig::currency: 'eth'

```

