require 'puppetlabs_spec_helper/module_spec_helper'
require 'rspec-puppet-utils'

# Uncomment this to show coverage report, also useful for debugging
#at_exit { RSpec::Puppet::Coverage.report! }

hiera_config_file = File.expand_path(File.join(File.dirname(__FILE__), 'fixtures','modules', 'crossbelt_data', 'hiera.yaml'))

RSpec.configure do |c|
    c.hiera_config =
    c.formatter = 'documentation'
    c.mock_with :rspec
end